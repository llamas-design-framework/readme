# About

*This document is a WIP.*

Llamas is an opinionated design framework for building gitlab-ci pipelines.

This document lays out a set of design guidelines on how to structure a gitlab-ci project.

## Why do I need this?

The intention of this design framework is to help provide core principles, structures, and techniques to set up and operate a gitlab-ci pipeline. It aims to help guide the solution space and in turn reduce complexity and wheel reinventing by structuring the problem-solving process. Teams following a design framework can focus more deeply on their project goals and reduce the needed efforts on the devops.

The main attributes of the llamas design framework for gitlab-ci pipelines is to design simple (no black magic) gitlab-ci pipeline structure that support extensibility, ability to divide into clean logical parts, and support a development environment where many team members collaborate in parallel.

## When do I not need this?

The llamas design framework for gitlab-ci is not intended for small and simple projects where it's best to simply have a single embedded gitlab-ci.yml file in the project repository. If the project is being developed by a smaller team, doesn't build many custom extensive tools to run within the pipeline, or is only a few trivial small steps, then do not use this design framework!

# Example

A basic set up of the design framework ideas is shown in the [example subproject](https://gitlab.com/llamas-design-framework/example).

```
example
|- numbers-project
|- numbers-project-pipeline
|- pipeline-containers
   |- number-cruncher
   |- random-number-generator
```

# Design Guidelines

## 1. Repository structuring

- Pipeline Repository
  - Separated repository for gitlab-ci pipeline
- Pipeline Configuration repository
  - Separated repository holding the non-secret pipeline configuration variables
- Job Container Repositories
  - Repositories for each job container

## 2. Pipeline Repository

## 3. Job Container Repositories

- Each job container has a CI (linting, testing, container building)
- Job container CI triggers pipeline to test itself

## 4. Configuration Repository

- Non-secret config stored as variables in this repo
- Code versioning applied makes it safer than CI/CD variables
- Avoids making config hardcoded into the pipeline repository
- Allows for others to re-use pipeline repository and bring their own config repository
- Can apply CI/CD to these config variables by triggering the pipeline repo in the CI
